import org.farng.mp3.MP3File
import java.io.File
import java.nio.file.Files
import java.nio.file.Paths
import java.util.*

fun main(args: Array<String>) {
    if (args.isEmpty()) println("Please enter valid path to folder as parameter")
    val folder = File(args[0])
    val files = folder.listFiles()
    val years = Array(files.size) { -1 }
    var i = 0
    for (file in files) {
        if (file.extension != "mp3") {
            continue
        }
        val mp3 = MP3File(file)

        if (mp3.hasID3v2Tag()) {
            val yearReleased = mp3.iD3v2Tag.yearReleased
            if (yearReleased.isNotBlank()) {
                years[i] = yearReleased.toInt()
            }
        }
        else if (mp3.hasID3v1Tag()) {
            val yearReleased = mp3.iD3v1Tag.yearReleased
            if (yearReleased.isNotBlank()) {
                years[i] = yearReleased.toInt()
            }
        }
        i++
    }

    years.sort()
    val outputLines = mutableListOf<CharSequence>()
    for (j in 0..GregorianCalendar()[Calendar.YEAR]) {
        val numSongsInYear = years.count { it == j }
        if (numSongsInYear == 0) continue
        outputLines.add("${j.toString()},$numSongsInYear")
    }
    val outputPath = Paths.get(folder.path + "/years.csv")
    Files.write(outputPath, outputLines)

}